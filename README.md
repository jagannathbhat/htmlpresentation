# htmlPresentation

A presentation made with HTML, CSS and JS.

## Instructions

1. Download all the files.
2. Open slide0.html in a web browser.
3. Press the right arrow key on your keyboard or the next button on your presentation remote to initiate the next animation.

**NOTE**: I built the presentation to be displayed on my laptop screen and no other screens. So it may render differently on your device.
